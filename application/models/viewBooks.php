<?php
/**
* 
*/
class viewBooks extends CI_Model
{
	
	function __construct()
	{
		parent:: __construct();

	}

	function view(){

		$this->db->select()->from('books');
		$query = $this->db->get();
		return $query->result_array();
	}

}

?>