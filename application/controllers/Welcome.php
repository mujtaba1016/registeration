	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

 	function __construct()
 	{
		parent::__construct();
 		$this->load->helper('form');
		$this->load->helper('html');
 	}
	public function index()
	{
		$this->load->view('reg_form');
	}
}
