<?php

/**
* 
*/
class helper extends CI_Controller      // helper is child class and CI_controller is parent class
{
	function __construct(){   // this is the constructer function of helper class

		parent::__construct();      // it calls the parent class 
		/*$this->load->helper('html');
		$this->load->helper('url');*/
		$this->load->helper(array('html','url','form'));
	
	}
	
	public function index()
	{
		$this->load->view('helper');
	}
}

?>