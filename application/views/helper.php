<?php

echo "<h1>This is h1 Heading by html code</h1>";

echo heading('This is h1 heading by helper function');

echo heading('This is h2 heading by helper function','2');

echo "<a href='https://www.google.com'>Google link by html a tag</a>";
echo "<br>";
echo anchor('https://www.google.com','Google link by function');
echo "<br>";

echo img('http://3.bp.blogspot.com/_4R1YTsjaAnk/TBKYmKB-pYI/AAAAAAAAGos/D7k6pSwhm68/s400/Arnold-Schwarzenegger-12.jpg','width="400"','height="400"');


//form generation by helpers

$attributes = array('Class' =>'css','name' =>'info','method' =>'get');
$inputs = array('class'=>'css','id'=>'css1','name'=>'fname','placeholder'=>'Name here');
$pas = array('type'=>'password','class'=>'css','name'=>'pass');
$options = array(
		'Beg'=>'Beginner',
		'int'=>'Intermediate',
		'pro'=>'Professional'
	);

echo form_open('action',$attributes);

echo form_label('First Name','name');
echo form_input($inputs);

echo form_label('Password','pass');
echo form_input($pas);

echo "<br>";
echo form_label('Password2','pass2');
echo form_password($pas);					// this is also used for password

echo form_dropdown('name',$options,'default');  //name is the name of dropdown options is the array of options and defualy value is the value if no option selected

echo form_submit('submit','SubmitForm');
echo form_close();
?>