<!DOCTYPE html>
<html>
<head>
	<title>Registeration form</title>
	<?php 
 		$this->load->helper('url');

	?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/style.css">
</head>
<body>
<div class="container">

<?php
	$attributes = array('id' => 'contact');
	echo form_open('',$attributes);
	echo heading('Simple Registeration Form');				
?><fieldset><?php		
	$attributes = array('Name' => 'Name','placeholder' => 'Your Name','tabindex' =>"1","required"=>"required","autofocus"=>"autofocus");
	echo form_input($attributes);
	echo "<br>";
?></fieldset><fieldset><?php

	$attributes = array('Name' => 'Email','type' => 'Email','placeholder' => 'Your Email Address','tabindex' =>"2","required"=>"required");
	echo form_input($attributes);
	echo "<br>";
?></fieldset><fieldset><?php

	$attributes = array('Name' => 'tel','type' => 'tel','placeholder' => 'Your Phone Number (optional)','tabindex' =>"3");
	echo form_input($attributes);
	echo "<br>";
?></fieldset><fieldset><?php

	$attributes = array('Name' => 'web','type' => 'url','placeholder' => 'Your Web Site (optional)','tabindex' =>"4");
	echo form_input($attributes);
	echo "<br>";
?></fieldset><fieldset><?php


	$attributes = array('name' => 'submit','type' => 'submit','id' => 'contact-submit','data-submit' =>'...Sending','value'=>'Submit');

	echo form_submit($attributes);

?></fieldset><?php
	
	echo form_close();
?>
	 
</div>



</body>
</html>

